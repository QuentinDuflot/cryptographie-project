GERMAN_LETTER_FREQUENCY = {'E': 17.41, 'N': 9.78, 'S': 7.89, 'I': 7.55, 'R': 7.00, 'A': 6.51, 'T': 6.15, 'D': 5.08, 'H': 4.76, 'U': 4.35, 'C': 3.06, 'L': 3.44, 'G': 3.01, 'M': 2.53,
                           'O': 2.51, 'B': 1.89, 'W': 1.89, 'F': 1.66, 'K': 1.21, 'Z': 1.13, 'P': 0.79, 'V': 0.67, 'J': 0.27, 'Y': 0.04, 'X': 0.03, 'Q': 0.02}

def readFile(fileName):
    file = open(fileName, 'r', encoding='utf-8-sig')
    text = file.read()
    file.close()
    return text

def writeFile(fileName, encryptedMessage):
    file = open(fileName, 'w', encoding='utf-8-sig')
    file.write(encryptedMessage)
    file.close()

def appendFile(fileName, encryptedMessage):
    file = open(fileName, 'a', encoding='utf-8-sig')
    file.write(encryptedMessage)
    file.close()

def generateTextWithGoodFrequencies(fileName):
    text = ''
    for letter,frequency in GERMAN_LETTER_FREQUENCY.items():
        numberOfLetters = int(frequency * 100)
        text += letter * numberOfLetters
    file = open(fileName, 'w', encoding='utf-8-sig')
    file.write(text)
    file.close()
