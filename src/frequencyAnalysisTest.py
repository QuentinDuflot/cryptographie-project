import os
from os.path import dirname
import unittest

from helpers import readFile
from frequencyAnalysis import bitsStringToBitsArray, getPositionsFrequencies, getCrackedMessage
from chiffre import normalizeMessage, toAsciiArray, asciiArrayToPositionArray, positionArrayTo8BitsArray, bitsArrayToString

class TestsFrequencyAnalysis(unittest.TestCase):
    def test_bitsStringToBitsArray(self):
        self.assertEqual(bitsStringToBitsArray('0000000111111111'), [[0, 0, 0, 0, 0, 0, 0, 1], [1, 1, 1, 1, 1, 1, 1, 1]])
    
    def test_getPositionsFrequencies(self):
        self.assertEqual(getPositionsFrequencies([1, 2, 1]), {1: 66.667, 2: 33.333})

    def test_decryptSimpleTextWithSimpleImage(self):
        imagePath = "26NuancesDeGrey.png"
        dirname = os.path.dirname(__file__)
        imageFullPath = os.path.join(dirname, imagePath)
        textFullPath = os.path.join(dirname, "ExampleText.txt")
        message = normalizeMessage(readFile(textFullPath))
        print(message)
        positionArray = asciiArrayToPositionArray(toAsciiArray(message), imageFullPath)
        bitsArray = positionArrayTo8BitsArray(positionArray)
        encryptedMessage = bitsArrayToString(bitsArray)

        print(getCrackedMessage(encryptedMessage))
        self.assertEqual(getCrackedMessage(encryptedMessage), message)
