import os
import random
import string
import sys
from copy import deepcopy
from random import shuffle
from helpers import generateTextWithGoodFrequencies, readFile, writeFile
from Image import Image

def main():
    imagePath = "PlusDeNuancesDeGrey.png"
    dirname = os.path.dirname(__file__)
    imageFullPath = os.path.join(dirname, imagePath)
    generateTextWithGoodFrequencies("ExampleText.txt")
    textFullPath = os.path.join(dirname, "ExampleText.txt")
    textWithGoodFrequencies = normalizeMessage(readFile(textFullPath))
    '''
    textToEncrypt = readFile("message.txt")
    cryptedTextWithGoodFrequencies = joinText(cryptText(normalizeMessage(textWithGoodFrequencies), createKey()))
    cryptedText = joinText(cryptText(normalizeMessage(textToEncrypt), createKey()))
    writeFile("encryptedMessage.txt", cryptedText)
    '''
    positionArray = asciiArrayToPositionArray(toAsciiArray(textWithGoodFrequencies), imageFullPath)
    bitsArray = positionArrayTo8BitsArray(positionArray)
    stringBits = bitsArrayToString(bitsArray)
    writeFile("encryptedMessageGoodFreq.txt", stringBits)


def createKey():
    alphabet = []
    for number in range(97,123):
        alphabet.append(chr(number))

    key = deepcopy(alphabet)
    shuffle(key)
    return key

def normalizeMessage(message):
    message = message.lower().replace('ü', 'ue').replace('ä', 'ae').replace('ö', 'oe').replace('ß', 'ss').replace(' ', '').replace('\n', '').translate(str.maketrans('', '', string.punctuation))
    return message

def cryptWord(word, key):
    result = str()
    for letter in word:
        result += key[ord(letter) - 97]
    return result

def cryptText(text, key):
    words = text.split()
    result = []
    for word in words:
        result.append(cryptWord(word, key))
    return result

def joinText(cryptedTextTab):
    result = str()
    for word in cryptedTextTab:
        result += ' ' + word
    return result

def toAsciiArray(string):
    return [ord(character) for character in list(string)]

def asciiArrayToPositionArray(asciiArray, imagePath):
    image = Image(imagePath)
    mapColors = image.colorsMap
    asciiArrayRange = range(len(asciiArray))
    for index in asciiArrayRange:
        asciiValue = asciiArray[index]
        if asciiValue in mapColors:
            asciiCoords = random.choice(mapColors[asciiValue])
            asciiArray[index] = image.getPositionFromCoords(asciiCoords)
        else:
            print("Color not present in image: Encryption not possible:" + str(asciiValue))
            sys.exit()
    return asciiArray

def intTo8BitsArray(integer):
    # https://stackoverflow.com/a/30971101/8583669
    return [1 if integer & (1 << (7-n)) else 0 for n in range(8)]

def positionArrayTo8BitsArray(positionArray):
    return [intTo8BitsArray(position) for position in positionArray]

def bitsArrayToString(bitsArray):
    bitsStringArray = [([''.join(str(bit)) for bit in bits]) for bits in bitsArray]
    chunck8BitsStringArray = [''.join(bits) for bits in bitsStringArray]
    return ''.join(chunck8BitsStringArray)

if __name__ == "__main__":
    main()