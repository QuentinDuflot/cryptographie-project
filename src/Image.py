from PIL import Image as ImageHelper
import math

class Image:
    def __init__(self, pathToImage):
        self.im = ImageHelper.open(pathToImage)
        self.pix = self.im.load()
        size = self.im.size
        self.width = size[0]
        self.height = size[1]
        self.colorsMap = self.getColorsMap()
    
    def getPixelAtCoord(self, coords):
        [x, y] = coords
        return self.pix[x,y]
    
    def getCoordsFromPosition(self, position):
        x = math.ceil(position / self.height)
        y = position % self.height
        #To justify in the Handout
        return [x - 1, y - 1]
    
    def getPositionFromCoords(self, coords):
        [x, y] = coords
        return (x * self.height) + y
    
    def getGreyValueInPixel(self, coords):
        return self.getPixelAtCoord(coords)[0]
    
    def getColorsMap(self):
        map = {}
        widthRange = range(self.width)
        heightRange = range(self.height)
        for col in widthRange:
            for row in heightRange:
                coords = [col, row]
                greyColor = self.getGreyValueInPixel(coords)
                if greyColor in map:
                    newPixelArray = map[greyColor]
                    newPixelArray.append(coords)
                    map[greyColor] = newPixelArray
                else:
                    map[greyColor] = [coords]
        return map
