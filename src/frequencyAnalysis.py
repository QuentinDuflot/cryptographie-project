from helpers import appendFile, readFile, writeFile
import operator
import math


GERMAN_LETTER_FREQUENCY = {'e': 17.41, 'n': 9.78, 's': 7.89, 'i': 7.55, 'r': 7.00, 'a': 6.51, 't': 6.15, 'd': 5.08, 'h': 4.76, 'u': 4.35, 'l': 3.44, 'c': 3.06,  'g': 3.01, 'm': 2.53,
                           'o': 2.51, 'b': 1.89, 'w': 1.89, 'f': 1.66, 'k': 1.21, 'z': 1.13, 'p': 0.79, 'v': 0.67, 'j': 0.27, 'y': 0.04, 'x': 0.03, 'q': 0.02}
lettersInFrequencyOrder = list(GERMAN_LETTER_FREQUENCY.keys())
LETTERS = 'abcdefghijklmnopqrstuvwxyz'
ENSIRA = 'ensiratdhuclgmobwfkzpvjyxq'


def main():
    '''
    textToDecrypt = normalizeMessage(readFile("encryptedMessageGoodFreq.txt"))
    letterCount = getLetterCount(textToDecrypt)
    decryptMessage = decryptMessageByFreq(textToDecrypt, letterCount, GERMAN_LETTER_FREQUENCY).lower()
    writeFile("decryptedByFrequency.txt", decryptMessage + "\n")
    arisne = sorted(GERMAN_LETTER_FREQUENCY.items(), key=operator.itemgetter(1))
    letterCount = getLetterCountReverse(textToDecrypt)
    decryptMessage = decryptMessageByFreq(textToDecrypt, letterCount, arisne).lower()
    appendFile("decryptedByFrequency.txt", "\n" + decryptMessage)
    
    print(stringArrayToString(asciiArrayToStringArray([97, 122, 101, 114, 116, 121])))
    print(bitsStringToBitsArray('0000000111111111'))
    print(bitsArrayToIntArray(bitsStringToBitsArray('0000000111111111')))
    '''
    textToDecrypt = normalizeMessage(readFile("encryptedMessageGoodFreq.txt"))
    print(getCrackedMessage(textToDecrypt))

    


def normalizeMessage(message):
    return message.lower()

def getAlphabetList():
    alphabet = []
    for letter in range(ord('a'), ord('z')):
        alphabet.append(chr(letter))
    return alphabet

def getDefaultLetterCountDict():
    letterCountDict = {}
    for letter in getAlphabetList():
        letterCountDict[letter] = 0
    return letterCountDict

def getLetterCount(message):
    letterCount = getDefaultLetterCountDict()
    for letter in message:
        if letter in LETTERS:
            letterCount[letter] += 1
    return sorted(letterCount.items(), key=operator.itemgetter(1) , reverse=True)

def getLetterCountReverse(message):
    letterCount = getDefaultLetterCountDict()
    for letter in message:
        if letter in LETTERS:
            letterCount[letter] += 1
    return sorted(letterCount.items(), key=operator.itemgetter(1))

def decryptMessageByFreq(message, letterCount, baseFrequency):
    for iteratorLetterFrequency, iteratorGermanLetterFrequency in zip(letterCount, baseFrequency):
        message = message.replace(iteratorLetterFrequency[0], iteratorGermanLetterFrequency[0])
    return message

def asciiArrayToStringArray(asciiArray):
    return [chr(ascii) for ascii in asciiArray]

def stringArrayToString(stringArray):
    return ''.join(stringArray)

def splitStringInGroups(string, n):
    # https://stackoverflow.com/a/9475354/8583669
    return [string[i:i+n] for i in range(0, len(string), n)]

def bitsStringToBitsArray(bitsString):
    bitsChunkArray = splitStringInGroups(bitsString, 8)
    return [([int(bitString) for bitString in list(bitsString)]) for bitsString in bitsChunkArray]

def bitsArrayToInt(bitsArray):
    # https://stackoverflow.com/a/12461400/8583669
    out = 0
    for bit in bitsArray:
        out = (out << 1) | bit
    return out

def bitsArrayToIntArray(bitsArray):
    return [bitsArrayToInt(bitsArray) for bitsArray in bitsArray]

def getPositionsFrequencies(positionArray):
    positionArrayLength = len(positionArray)
    positionFrequencies = {}
    for position in positionArray:
        if position in positionFrequencies:
            positionFrequencies[position] += 1
        else:
            positionFrequencies[position] = 1
    for occurence in positionFrequencies:
        positionFrequencies[occurence] = round((positionFrequencies[occurence] * 100) / positionArrayLength, 3)
    return positionFrequencies
'''
Refactor this shit!!!!!!!!!!!!!!!!!
'''

'''
def getLetterFromFrequency(frequency):
    closestLetter = ['dummy', 1000]
    for letter in GERMAN_LETTER_FREQUENCY:
        frequencyDistance = abs(frequency - GERMAN_LETTER_FREQUENCY[letter])
        if frequencyDistance < closestLetter[1]:
            closestLetter = [letter, frequencyDistance]
    return closestLetter[0]
'''

def getSortedPositionFrequencies(positionFrequencies):
    sortedPositionsTuple = sorted(positionFrequencies.items(), key=operator.itemgetter(1), reverse=True)
    positionKey, positionValue = zip(*sortedPositionsTuple)
    return positionKey

def getPositionLetterEquivalenceDict(positions):
    positionFrequencies = getPositionsFrequencies(positions)
    sortedPositionFrequencies = getSortedPositionFrequencies(positionFrequencies)
    # TODO: Handle case where there is not 26 frequencies
    equivalence = {}
    for i in range(len(sortedPositionFrequencies)):
        equivalence[sortedPositionFrequencies[i]] = lettersInFrequencyOrder[i]
    print(sortedPositionFrequencies)
    return equivalence

def getSortedPositionsByApparition(positionsArray):
    positionsFrequency = {i:positionsArray.count(i) for i in positionsArray}
    return getSortedPositionFrequencies(positionsFrequency)
    

def getCrackedMessage(encryptedMessage):
    positionsInMessage = bitsArrayToIntArray(bitsStringToBitsArray(encryptedMessage))
    sortedPositionsByApparition = getSortedPositionsByApparition(positionsInMessage)
    positionCount = len(sortedPositionsByApparition)
    averagePositionPerLetter = positionCount/26
    averagePositionPerLetter = math.ceil(averagePositionPerLetter)
    positionLetterEquivalence = {}
    for i in range(len(lettersInFrequencyOrder)):
        for j in range(i*averagePositionPerLetter, i*averagePositionPerLetter + averagePositionPerLetter):
            if j >= positionCount:
                positionLetterEquivalence[sortedPositionsByApparition[j]] = lettersInFrequencyOrder[i]

    crackedMessage = ''
    for position in positionsInMessage:
        crackedMessage += positionLetterEquivalence[position]
    return crackedMessage

if __name__ == "__main__":
    main()
